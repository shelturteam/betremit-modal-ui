# README #

This README outlines the steps to take to setup and run this application.

### What is this repository for? ###

* Betremit payment modal UI
* 1.0.0

### How do I get set up? ###

- Clone the repo.
- Run `npm install`.
- Run `node server.js`.
